package toString;

public class Automovil {
	private String marca;
	private String modelo;
	private String color;
	private int numAsientos = 5;
	private boolean seminuevo = false;
	
	public Automovil(String marca, String modelo, String color, int numAsientos) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;
		this.numAsientos = numAsientos;
	}

	@Override
	public String toString() {
		return "Automovil [marca=" + marca + ", modelo=" + modelo + ", color=" + color + ", numAsientos=" + numAsientos
				+ "]";
	}

	//Conversión de tipos primitivos a String
	public String getSeminuevo() {
		String seminuevo = Boolean.toString(this.seminuevo);
		String numAsientos = Integer.toString(this.numAsientos);
		
		if(seminuevo == "true")
			return "Automovil SEMINUEVO" + numAsientos;
		else 
			return "Automóvil NUEVO" + numAsientos;
	}

	public boolean isSeminuevo() {
		return seminuevo;
	}

	public void setSeminuevo(boolean seminuevo) {
		this.seminuevo = seminuevo;
	}
}
