package Interfaces;

public interface Volador {
	
	public abstract void despegar();
	
	public abstract void volar();
}
