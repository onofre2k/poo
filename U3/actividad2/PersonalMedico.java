package U3Act2;

import java.util.ArrayList;
import java.util.Scanner;

public class PersonalMedico {
	static Scanner entrada = new Scanner(System.in);
	static ArrayList<Persona> persona = new ArrayList<Persona>();
	
	public static void main(String[] args) {
		int opcion1 = 0;
		do {
			System.out.println("******* Menú *******");
			System.out.println("1.Registrar Usuario\n2.Mostrar Datos\n3.Salir");
			System.out.print("\nOpcion: ");
			opcion1 = entrada.nextInt();
			
			switch (opcion1) {
				case 1:
					guardarRegistro();
					break;
				
				case 2:
					mostrarDatos();
					break;
	
				case 3:
					System.out.println("Hasta luego! Muchas gracias.");
					break;
					
				default:
					System.out.println("Opción inválida..\n");
					break;
			}
			
		} while(opcion1 != 3);

	}
	
	public static void guardarRegistro() {
		int opcion = 0;
		boolean regresar = false;
		do {
			System.out.println("\nTipo de Usuario: \n1. Médico\n2. Paciente\n3. Enfermera\n4. Regresar");
			System.out.print("\nOpción: ");
			opcion = entrada.nextInt();
			
			if(opcion > 0 && opcion < 4) {
				String nombre, apellidos;
				int edad;
					
				System.out.print("\nProporciona nombre: ");
				nombre = entrada.next();
				System.out.print("Proporciona apellidos: ");
				apellidos = entrada.next();
				System.out.print("Proporciona la edad: ");
				edad = entrada.nextInt();
				
				if(edad >= 18) {
					switch (opcion) {
						case 1:
							String cedulaProfesional;
							System.out.print("Proporciona la cédula profesional: ");
							cedulaProfesional = entrada.next();
							Medico medico = new Medico(nombre, apellidos, edad, cedulaProfesional);
							persona.add(medico);
							break;
							
						case 2:
							String numExpediente;
							System.out.print("Proporciona número de expediente: ");
							numExpediente = entrada.next();
							Paciente paciente = new Paciente(nombre, apellidos, edad, numExpediente);
							persona.add(paciente);
							break;
							
						case 3:
							String especialidad;
							System.out.println("Proporciona la especialidad: ");
							especialidad = entrada.next();
							Enfermera enfermera = new Enfermera(nombre, apellidos, edad, especialidad);
							persona.add(enfermera);
							break;
							
					}
				} else {
					System.out.println("\nError: La edad mínima es 18 años");
				}
				System.out.println();
				
			} else if(opcion == 4){
				regresar = true;
				System.out.println();
			} else
				System.out.println("\nOpción inválida");
		} while(opcion > 4 ^ !regresar);
	}
	
	public static void mostrarDatos() {
		if(persona.size() > 0) {
			System.out.println("\nLos datos de los usuarios registrados son: ");
			for (Persona persona2 : persona) {
//				if(persona2 instanceof Medico) {
//					Medico medico = (Medico) persona2;
//					System.out.println(medico.listarDatos());
//				} else if(persona2 instanceof Paciente) {
//					Paciente paciente = (Paciente) persona2;
//				}
				System.out.println(persona2.listarDatos());
			}
		} else
			System.out.println("\nNo hay datos registrados!");
		
		System.out.println();
	}
}
