package U3Act2;

public class Enfermera extends Persona {
	private String especialidad;
	private final int TIPO = 3;
	
	public Enfermera(String nombre, String apellidos, int edad, String especialidad) {
		super(nombre, apellidos, edad);
		this.especialidad = especialidad; 
	}

	@Override
	public String listarDatos() {
		return "Enfermera: [especialidad= " + especialidad + ", nombre= " + nombre + ", apellidos= " + apellidos + ", edad= "
				+ edad + "]";
		
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public int getTIPO() {
		return TIPO;
	}
	
}
