package toString;

public class AutomovilController {
	
	public static void main(String[] args) {
		Automovil automovil = new Automovil("Mazda 3", "2010", "Gris", 4);
		
		System.out.println("Los datos del Automóvil son: ");
		System.out.println(automovil.toString());
		
		System.out.println("\n\rConversión de tipo primitivo a String: ");
		System.out.println("Seminuevo: " + automovil.isSeminuevo());
		automovil.setSeminuevo(true);
		System.out.println("Seminuevo: " + automovil.getSeminuevo());
	}
}
