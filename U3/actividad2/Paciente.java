package U3Act2;

public class Paciente extends Persona {
	private String numExpediente;
	private final int TIPO = 2;
	
	public Paciente(String nombre, String apellidos, int edad, String numExpediente) {
		super(nombre, apellidos, edad);
		this.numExpediente = numExpediente;
	}

	@Override
	public String listarDatos() {
		return "Paciente: [numExpediente= " + numExpediente + ", nombre= " + nombre + ", apellidos= " + apellidos
				+ ", edad= " + edad + "]";
		
	}
	

	public String getNumExpediente() {
		return numExpediente;
	}

	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}

	public int getTIPO() {
		return TIPO;
	}
	
}
