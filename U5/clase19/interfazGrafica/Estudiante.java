package interfazGrafica;

public class Estudiante {
	private String nombre;
	private int edad;
	private String numCuenta;
	
	public Estudiante(String nombre, int edad, String numCuenta) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.numCuenta = numCuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	
	public String toString() {
		return this.nombre;
	}

}
