
public class FechaController {

	public static void main(String[] args) {
		Fecha fecha1 = new Fecha(17, 9, 1982);
		Fecha fecha2 = new Fecha(17, 9, 1982);
		
		if(fecha1 == fecha2) {
			System.out.println("Fecha1 es IDÉNTICA a Fecha2");
		} else {
			System.out.println("Fecha1 NO es IDÉNTICA a Fecha2");
		}
		
		if(fecha1.equals(fecha2)) {
			System.out.println("Fecha1 es IGUAL a Fecha 2");
		} else {
			System.out.println("Fecha1 NO es IGUAL a Fecha 2");
		}
		
		System.out.println("\n\rCambiando el objeto fecha 2\n\r");
		
		fecha2 = fecha1;
		
		if(fecha1 == fecha2)
			System.out.println("Fecha1 es IDÉNTICA a Fecha2");
		else
			System.out.println("Fecha1 NO es IDÉNTICA a Fecha2");

	}
	
}
