package Interfaces;

public class Pajaro extends Animal implements Volador {

	
	@Override
	public void despegar() {
		System.out.println("El pájaro se hecho a volar");
		
	}

	@Override
	public void volar() {
		System.out.println("El pájaro esta volando todos los días");
		
	}

	@Override
	public void comer() {
		System.out.println("Es hora de comer!");
		
	}


}
