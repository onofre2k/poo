
public class EquipoFutbol {

	public static void main(String[] args) {
		
		Empleado empleado = new Empleado();
		
		Entrenador entrenador = new Entrenador("Armando", "Onofre", 1);
		Jugador jugador = new Jugador("Hugo", "Sanchez", 2);
		
//		Masajista masajista = new Masajista();
		
		empleado.mostrarTipoEmpleado(empleado);
		
		//Sobrecarga de Métodos
		jugador.jugar();
		jugador.jugar("Mediocampista");

	}

}
