package Interfaces;

public class MedioTransporte {
	protected int numAsientos = 0;
	
	
	public MedioTransporte(int numAsientos) {
		super();
		this.numAsientos = numAsientos;
	}
	

	public void abordarPasajeros() {
		System.out.println("Los pasajeros están abordando");
	}

}
