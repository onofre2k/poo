package U3Act2;

public class Medico extends Persona {
	private String cedulaProfesional;
	private final int TIPO = 1;

	public Medico(String nombre, String apellidos, int edad, String cedulaProfesional) {
		super(nombre, apellidos, edad);
		this.cedulaProfesional = cedulaProfesional;
	}

	@Override
	public String listarDatos() {
		return "Medico: [cedulaProfesional= " + cedulaProfesional + ", nombre= " + nombre + ", apellidos= " + apellidos
				+ ", edad= " + edad + "]";
		
	}

	public String getCedulaProfesional() {
		return cedulaProfesional;
	}

	public void setCedulaProfesional(String cedulaProfesional) {
		this.cedulaProfesional = cedulaProfesional;
	}

	public int getTIPO() {
		return TIPO;
	}
	
}
