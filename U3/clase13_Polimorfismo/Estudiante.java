
public class Estudiante extends Persona {
	private String numCuenta;
	
	public Estudiante(String nombre, String apellidos, int edad) {
		super(nombre, apellidos, edad);
	}

	@Override
	public void asistirCampus() {
		// TODO Auto-generated method stub
		System.out.println("El estudiante asistió al campus!");
		
	}
	
	public void estudiarExamen() {
		System.out.println("El estudiante estudió para su exámen");
	}

	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

}
