
public class Jugador extends Empleado {

	public Jugador(String nombre, String apellidos, int numEmpleado) {
		super(nombre, apellidos, numEmpleado);
	}
	
	public void jugar() {
		System.out.println("\n\rEl jugador "
				+ super.nombre + " se encuentra jugando en el campo");
	}
	
	//Método sobrecargado
	public void jugar(String posicion) {
		System.out.println("El jugador "
				+ super.nombre + " va a jugar en la posicion de "
				+ posicion);
	}

}
