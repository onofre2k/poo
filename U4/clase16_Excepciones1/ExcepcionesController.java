import java.util.InputMismatchException;
import java.util.Scanner;

public class ExcepcionesController {
	static Scanner entrada = new Scanner(System.in);
	static int num1, num2;
	static boolean bandera = true;
	
	public static void main(String[] args) {
		do {	
			try {
		
					pedirNumeros();
							
					System.out.println("El resultado de la división es: "+getDivision());
					
					bandera = false;
			} catch (InputMismatchException inputMismatchException) {
				System.err.println("Excepción: " + inputMismatchException);
				entrada.nextLine();
				System.out.println("\nSolo se aceptan valores enteros. Intente nuevamente");
			} catch(ArithmeticException arithmeticException) {
				System.err.println("Excepción: " + arithmeticException);
				
				System.out.println("No se puede realizar división by zero. Intente nuevamente\n");
			}
		}while(bandera);

	}
	
	//Calcular la división de 2 números
	public static double getDivision() {
		return num1/num2;
	}
	
	public static void pedirNumeros() throws InputMismatchException {
		System.out.print("Proporcione un número entero 1: ");
		num1 = entrada.nextInt();
		System.out.print("Proporcione un número entero 2: ");
		num2 = entrada.nextInt();
	}

}
