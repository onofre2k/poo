package interfazGrafica;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

public class InterfazGrafica extends JFrame {
	public JPanel panel;
	public JTextField textNombre;
	public JLabel label2;
	public JButton boton1;
	public JTextArea area;
	public JButton boton2;

	public InterfazGrafica() {
		this.setSize(new Dimension(500, 500));
		this.setTitle("Ventana 1");
		//this.setLocation(200, 200);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		//this.setBounds(200, 200, 500, 500);
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setMinimumSize(new Dimension(400, 300));
		
		construirComponentes();
		
	}
	
	private void construirComponentes() {
		crearPaneles();
		//crearEtiquetas();
		//crearBotones();
		//crearRadioBotones();
		//crearToggleBotones();
		crearCamposTexto();
		crearAreasTexto();
		//crearCasillasVerifica();
		//crearListaDesplegable();
		//crearCampoPassword();
		//crearTabla();
		//crearLista();
	}
	
	private void crearPaneles() {
		panel = new JPanel();
		this.getContentPane().add(panel);
		//panel.setBackground(Color.DARK_GRAY);
		panel.setLayout(null);
		
		//crearMouseMotionListener();
	}
	
	private void crearEtiquetas() {
		//Etiqueta tipo Texto
		JLabel label1 = new JLabel();
		label1.setBounds(50, 100, 100, 30);
		label1.setOpaque(true);
		label1.setForeground(Color.WHITE);
		label1.setBackground(Color.DARK_GRAY);
		label1.setFont(new Font("Times new roman", 3, 16));
		
		label1.setText("Nombre: ");
		label1.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(label1);
		
		label2 = new JLabel();
		label2.setBounds(50, 180, 200, 40);
		panel.add(label2);
	}
	
	private void crearBotones() {
		boton1 = new JButton("Guardar");
		boton1.setBounds(130, 300, 100, 40);
		panel.add(boton1);
		
		boton2 = new JButton("Listo");
		boton2.setBounds(250, 300, 100, 40);
		panel.add(boton2);
		
		crearActionListener();
		crearMouseListener();
	}

	private void crearRadioBotones() {
		JRadioButton radio1 = new JRadioButton("Opcion 1", true);
		radio1.setBounds(50, 80, 100, 30);
		panel.add(radio1);
		
		JRadioButton radio2 = new JRadioButton("Opcion 2", false);
		radio2.setBounds(50, 130, 100, 30);
		panel.add(radio2);
		
		JRadioButton radio3 = new JRadioButton("Opcion 3", false);
		radio3.setBounds(50, 180, 100, 30);
		panel.add(radio3);
		
		ButtonGroup grupo = new ButtonGroup();
		grupo.add(radio1);
		grupo.add(radio2);
		grupo.add(radio3);
	}

	private void crearToggleBotones() {
		JToggleButton toggle1 = new JToggleButton("Opcion1", true);
		toggle1.setBounds(50, 80, 100, 30);
		panel.add(toggle1);

		JToggleButton toggle2 = new JToggleButton("Opcion2", false);
		toggle2.setBounds(50, 130, 100, 30);
		panel.add(toggle2);
		
		ButtonGroup grupo = new ButtonGroup();
		grupo.add(toggle1);
		grupo.add(toggle2);
		
		System.out.println(toggle1.getText());
	}

	private void crearCamposTexto() {
		textNombre = new JTextField();
		textNombre.setBounds(150, 60, 200, 30);
		//textNombre.setText("Proporciona Nombre");
		panel.add(textNombre);
		
		System.out.println(textNombre.getText());
		
		crearKeyListener();
	}
	
	private void crearAreasTexto() {
		area = new JTextArea();
		area.setBounds(100, 100, 250, 200);
		panel.add(area);
		
		JScrollPane scroll = new JScrollPane(area, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setBounds(100, 100, 250, 200);
		panel.add(scroll);
	}
	
	private void crearCasillasVerifica() {
		JCheckBox casilla1 = new JCheckBox("Opcion 1", true);
		casilla1.setBounds(50, 100, 100, 30);
		casilla1.setEnabled(false);
		panel.add(casilla1);
		
		JCheckBox casilla2 = new JCheckBox("Opcion 2", false);
		casilla2.setBounds(50, 150, 100, 30);
		panel.add(casilla2);
	}

	private void crearListaDesplegable() {
		Estudiante estudiante1 = new Estudiante("Armando Onofre", 34, "87687678678");
		Estudiante estudiante2 = new Estudiante("Federico Huerta", 23, "342342424");
		Estudiante estudiante3 = new Estudiante("Daniela Reyes", 18, "54353453");
		
		
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		model.addElement(estudiante1);
		model.addElement(estudiante2);
		model.addElement(estudiante3);
		
		JComboBox listaDesplega = new JComboBox(model);
		listaDesplega.setBounds(50, 100, 250, 40);
		listaDesplega.setSelectedItem(estudiante2);
		panel.add(listaDesplega);
		
		Estudiante estudiante4;
		estudiante4 = (Estudiante) listaDesplega.getSelectedItem();
		
		System.out.println("No. Cuenta: "+estudiante4.getNumCuenta());
	}
	
	private void crearCampoPassword() {
		JPasswordField password = new JPasswordField("Hola12345678");
		password.setBounds(50, 100, 100, 30);
		panel.add(password);
		
		System.out.println(password.getPassword());
	}

	private void crearTabla() {
		
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.addColumn("Nombre");
		modelo.addColumn("Edad");
		modelo.addColumn("No. Cuenta");
		
		modelo.addRow(new Object[] {"Armando Onofre", 34, "876876786"});
		modelo.addRow(new Object[] {"Gerardo Lopez", 23, "43242424"});
		modelo.addRow(new Object[] {"María Gutierrez", 26, "423424242"});
		
		JTable tabla = new JTable(modelo);
		tabla.setBounds(100, 100, 300, 250);
		panel.add(tabla);
		
		JScrollPane scroll = new JScrollPane(tabla, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setBounds(100, 100, 300, 250);
		panel.add(scroll);

	}
	
	private void crearLista() {
		Estudiante estudiante1 = new Estudiante("Armando Onofre", 34, "87687678678");
		Estudiante estudiante2 = new Estudiante("Federico Huerta", 23, "342342424");
		Estudiante estudiante3 = new Estudiante("Daniela Reyes", 18, "54353453");
		
		DefaultListModel model = new DefaultListModel();
		model.addElement(estudiante1);
		model.addElement(estudiante2);
		model.addElement(estudiante3);
		
		JList lista = new JList(model);
		lista.setBounds(100, 100, 200, 250);
		lista.setSelectedValue(estudiante2, true);
		panel.add(lista);
		
		Estudiante estudiante4;
		estudiante4 = (Estudiante) lista.getSelectedValue();
		
		System.out.println("No. Cuenta: "+estudiante4.getNumCuenta());
	}
	
	private void crearActionListener() {
//		ActionListener accionBoton1 = new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				// TODO Auto-generated method stub
//				label2.setText("El nombres es: "+textNombre.getText());
//			}
//		};
//		
//		boton1.addActionListener(accionBoton1);
		
		ActionListener accionBoton2 = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				area.setText(null);
			}
		};
		
		boton2.addActionListener(accionBoton2);
	}
	
	private void crearMouseListener() {
		MouseListener accionMouse1 = new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
				//area.append("mouseReleased\n");
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				//area.append("mousePressed\n");
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				//area.append("mouseExited\n");
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				//area.append("mouseEntered\n");
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
				if(e.isAltDown())
					area.append("Se oprimió la tecla Alt\n");
				else if(e.isControlDown())
					area.append("Se oprimió la tecla Control\n");
				else if(e.isMetaDown())
					area.append("Clic Derecho\n");
				else if(e.getClickCount() == 2)
					area.append("Doble Click\n");
				else
					area.append("Clic Izquierdo\n");
			}
		};
		
		boton1.addMouseListener(accionMouse1);
	}
	
	private void crearMouseMotionListener() {
		MouseMotionListener accionMouse2 = new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				area.append("Movimiento del Mouse\n");
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				area.append("El mouse se esta arrastrando\n");
			}
		};
		
		panel.addMouseMotionListener(accionMouse2);
	}
	
	private void crearKeyListener() {
		KeyListener teclado = new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				//area.append("keyTyped\n");
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				area.append("keyReleased\n");
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				area.append("keyPressed\n");
			}
		};
		
		textNombre.addKeyListener(teclado);
	}

}
