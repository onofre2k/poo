package Interfaces;

public class InterfacesController {
	public static void main(String[] args) {
		
		System.out.println("El comportamiento del Pájaro es:");
		Pajaro pajaro = new Pajaro();
		pajaro.despegar();
		pajaro.volar();
		pajaro.comer();
		
		
		System.out.println("\nEl comportamiento del Avión es:");
		Avion avion = new Avion(45);
		avion.abordarPasajeros();
		avion.despegar();
		avion.volar();
		

	}
}
