
public class Estudiante extends Persona {
	private String numCuenta;
	
	public Estudiante() {
		super();
	}
	
	public Estudiante(String nombre, String apellidos) {
		super(nombre, apellidos);
		// TODO Auto-generated constructor stub
	}
	
	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
}
