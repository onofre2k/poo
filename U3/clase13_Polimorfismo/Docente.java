
public class Docente extends Persona {
	private String numExpediente;
	
	public Docente(String nombre, String apellidos, int edad) {
		super(nombre, apellidos, edad);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void asistirCampus() {
		// TODO Auto-generated method stub
		System.out.println("El docente acudió al campus!");
	}
	
	public void prepararClase() {
		System.out.println("El docente preparó su clase!");
	}

	public String getNumExpediente() {
		return numExpediente;
	}

	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}

	@Override
	public void cobrar() {
		// TODO Auto-generated method stub
		System.out.println("El docente recibió su pago!");
	}
}
