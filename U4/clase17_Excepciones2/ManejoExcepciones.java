import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ManejoExcepciones {

	public static void main(String[] args) {
		try {
			leerDocumento();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al leer el archivo");
		} finally {
			System.out.println("Se ejecuto el bloque del documento");
		}
		
		try {
			operacion();
		} catch (ArithmeticException ea) {
			System.out.println("División entre 0");
		}
		
		try {
			pedirNumero();
		} catch (ExcepcionPersonalizada ep) {
			System.out.println("El usuario ingreso el número 0");
		}
		
		System.out.println("La Aplicación Continúa");

	}
	
	/*
	 * Excepciones verificadas con throws
	 */
	public static void leerDocumento() throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader("/Users/onofre2k/Documents/CLASES/POO/pruebaDocumento.txt");
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            System.out.println(cadena);
        }
        b.close();
	}
	
	public void leerDocumentoDos() throws FileNotFoundException, IOException {
		leerDocumento();
	}
	
	/*
	 * Excepciones no verificadas
	 */
	public static double operacion() {
		int num1 = 8, num2 = 0;
		
		return num1/num2;
	}
	
	/*
	 * Excepciones Personalizadas por medio de throw
	 */
	public static void pedirNumero() throws ExcepcionPersonalizada {
		Scanner entrada = new Scanner(System.in);
		int numero;
		do {
			System.out.println("Proporcione un numero: ");
			numero = entrada.nextInt();
			
			if(numero == 0)
				throw new ExcepcionPersonalizada();
			
		} while(numero!= 0);
	}
	/*
	 * Lanzar excepciones mediante throw con bloque finally
	 */
	public static void lanzarExcepcion() throws Exception {

	}

	/*
	 * Bloque finally sin excepciones
	 */
	public static void noLanzarExcepcion() {

	}
}
