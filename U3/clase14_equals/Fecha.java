/*
 * Clase para la sobrescritura del método equals de la clase Object
 */
public class Fecha {
	private int dia;
	private int mes;
	private int anio;
	
	public Fecha(int dia, int mes, int anio) {
		super();
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean resultado = false;
		if((o != null) && (o instanceof Fecha)) {
			Fecha fecha = (Fecha) o;
			if((dia == fecha.dia) && (mes == fecha.mes)
					&& (anio == fecha.anio)) {
				resultado = true;
			}
		}
		return resultado;
	}
	
	@Override
	public int hashCode() {
		return (dia ^ mes ^ anio);
	}
}
