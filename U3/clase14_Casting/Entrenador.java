
public class Entrenador extends Empleado {
	private int aniosExperiencia = 0;
	
	public Entrenador(String nombre, String apellidos, int numEmpleado) {
		super(nombre, apellidos, numEmpleado);
	}

	public int getAniosExperiencia() {
		return aniosExperiencia;
	}

	public void setAniosExperiencia(int aniosExperiencia) {
		this.aniosExperiencia = aniosExperiencia;
	}
	
}
