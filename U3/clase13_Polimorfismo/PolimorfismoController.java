
public class PolimorfismoController {

	public static void main(String[] args) {
		
		Estudiante estudiante = new Estudiante("Mario", "García", 18);
		Docente docente = new Docente("Armando", "Onofre", 38);
		Administrativo administrativo = new Administrativo("Luis", "Reyes", 56);
		
		System.out.println("Todas las personas asistieron al campus: ");
		estudiante.asistirCampus();
		docente.asistirCampus();
		administrativo.asistirCampus();
		
		System.out.println("Todas las personas cobraron: ");
		estudiante.cobrar();
		docente.cobrar();
		administrativo.cobrar();
		
		System.out.println("Sus actividades: ");
		estudiante.estudiarExamen();
		docente.prepararClase();
		administrativo.generarNomina();
		
	}

}
