
public class Empleado {
	protected String nombre;
	protected String apellidos;
	protected int numEmpleado;
	
	public Empleado() {
		
	}
	
	//Constructor sobrecargado
	public Empleado(String nombre, String apellidos, int numEmpleado) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.numEmpleado = numEmpleado;
	}
	
	//Operador instanceof
	public void mostrarTipoEmpleado(Empleado e) {
		if(e instanceof Entrenador) {
			Entrenador entrenador = (Entrenador) e; //Casting Objects
			System.out.println("El empleado "
					+ entrenador.nombre + " es Entrenador");
		} else if(e instanceof Jugador) {
			Jugador jugador = (Jugador) e; //Casting Objects
			System.out.println("El empleado "
					+ jugador.nombre + " es Jugador");
		} else
			System.out.println("El empleado es de otro tipo");
	}
	
}
