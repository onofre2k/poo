package recursos_humanos;

public class Docente extends Empleado {
	private String cedula;
	private String grado;
	
	public Docente(int numEmpleado, String nombre, String apellidos, boolean activo) {
		super(numEmpleado, nombre, apellidos, activo);
	}
	
	public void registrarDocente() {
		System.out.println("Datos del Docente\n\r" + "Nombre: " +
				nombre + " " + apellidos + "\n\rCédula Profesional: " + 
				getCedula() + "\n\rGrado: " + getGrado());
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}
}
