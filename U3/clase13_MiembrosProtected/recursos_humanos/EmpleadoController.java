package recursos_humanos;
public class EmpleadoController {

	public static void main(String[] args) {
		
		Docente docente = new Docente(34567, "Armando", "Onofre Martínez", true);
		docente.registrarDocente();
		
		Administrativo administrativo = new Administrativo();
		administrativo.registrarAdministrativo();
		
		Empleado empleado = new Empleado();
		System.out.println("\n\rEl número de empleado protected es: " +
					empleado.numEmpleado);
		
	}

}
