package Interfaces;

public class Avion extends MedioTransporte implements Volador {

	public Avion(int numAsientos) {
		super(numAsientos);
	}

	@Override
	public void despegar() {
		System.out.println("El avión esta despegando");
	}

	@Override
	public void volar() {
		System.out.println("El avión esta volando");
	}
	
}
