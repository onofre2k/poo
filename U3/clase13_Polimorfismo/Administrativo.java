
public class Administrativo extends Persona {
	private String numTrabajador;
	
	public Administrativo(String nombre, String apellidos, int edad) {
		super(nombre, apellidos, edad);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void asistirCampus() {
		// TODO Auto-generated method stub
		System.out.println("El administrativo asistió al campus!");
	}
	
	public void generarNomina() {
		System.out.println("El administrativo generó la nomina del mes!");
	}

	public String getNumTrabajador() {
		return numTrabajador;
	}

	public void setNumTrabajador(String numTrabajador) {
		this.numTrabajador = numTrabajador;
	}

	@Override
	public void cobrar() {
		// TODO Auto-generated method stub
		System.out.println("El administrativo ya cobró!");
	}

}
